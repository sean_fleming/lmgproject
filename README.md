# LMGproject
View the READ.ME

VR project, trying to create a demo scene from the book "the little match girl",
I would like little movement of the character, if you click on the scale of the base box,
the scale is about (x:20 y:20 z:2).  the goal is to create a project that feels a bit immersive, 
the environment is snowy, the protagonist has long hair, female, and only interaction in the game
besides movement is lighting a match.  boxes are fine to start, but I would like to simulate a 
character who loses a slipper to 2 speeding carriages, and limps coldly to a snowy corner, then the 
light interaction starts.  We can build slowly but this would be a nice introductory project to a VR
industry I definitely have a feel for.

Any help would be appreciated but I'm really just trying to keep this a solo project.
